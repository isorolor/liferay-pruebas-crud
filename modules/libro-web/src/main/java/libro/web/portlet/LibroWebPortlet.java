package libro.web.portlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.SecureRandom;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;

import libro.model.Escritor;
import libro.model.Libro;
import libro.service.EscritorLocalService;
import libro.service.EscritorLocalServiceUtil;
import libro.service.LibroLocalService;
import libro.service.LibroLocalServiceUtil;
import libro.web.constants.LibroWebPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=CRUD-Escritores",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=libro-web Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + LibroWebPortletKeys.LibroWeb, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class LibroWebPortlet extends MVCPortlet {

	private static final int caracteres = 75;
	private EscritorLocalService _escritorLocalService;
	private LibroLocalService _libroLocalService;

	@Reference
	public void setEscritorLocalService(EscritorLocalService escritorLocalService) {
		_escritorLocalService = escritorLocalService;
	}

	@Reference
	public void setLibroLocalService(LibroLocalService libroLocalService) {
		_libroLocalService = libroLocalService;
	}

	public void addEscritor(ActionRequest request, ActionResponse response) throws IOException, PortletException, PortalException {

		ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String nombre = ParamUtil.getString(request, "nombreEscritor");
		String nacionalidad = ParamUtil.getString(request, "nacionalidad");

		long fotoEscritorId = fileUpload(td, request);
		Escritor escritor = _escritorLocalService.fetchByNombre(nombre);

		if (escritor == null) {
			if (nacionalidad != null && nacionalidad.length() < caracteres) {
				_escritorLocalService.addEscritor2(td.getSiteGroupId(), td.getCompanyId(), td.getUser().getUserId(),
						td.getUser().getFullName(), nombre, nacionalidad, fotoEscritorId);
				response.setRenderParameter("mvcPath", "/listarEscritores.jsp");
			}

		} else {
			SessionErrors.add(request, "escritor-exist");
			SessionMessages.add(request,
					PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			response.setRenderParameter("mvcPath", "/addEscritor.jsp");
		}

	}

	private long fileUpload(ThemeDisplay td, ActionRequest request) {
		// Fichero con nombre aleatorio:
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		// Renombrado aqui:
		String fileName = uploadPortletRequest.getFileName("uploadedFile");
		//Se crea un objeto file con el fichero y el nombre
		File file = uploadPortletRequest.getFile("uploadedFile");
		// -->System.out.println(file.getName());
		String mimeType = uploadPortletRequest.getContentType("uploadedFile");
		// -->System.out.println(mimeType);
		String title = fileName;
		// filename unico
		String description = "Fichero subido mediante formulario para a�adir escritor";
		long repositoryId = td.getScopeGroupId();
		//Es con getScopeId(); si pongo getCompanyId() hace todo el proceso pero no sube el archivo a Docs&Media
		
		System.out.println("Title=>" + title);
		System.out.println("RepositoryID=>" + repositoryId);
		long fotoEscritorId = 0;
		
		//Falta comprobar que si la foto ya est� almacenada en Docs&media, en ese caso la renombre y la suba igualmente.
		String cadenaAleatoria = generateRandomText();
		title = title + "-" + cadenaAleatoria;
		
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request);
			InputStream is = new FileInputStream(file);
			DLAppServiceUtil.addFileEntry(repositoryId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, fileName, mimeType,
					title, description, "", is, file.length(), serviceContext);
			
			FileEntry fl = DLAppServiceUtil.getFileEntry(repositoryId, 0, title);
			fotoEscritorId = fl.getFileEntryId();
			System.out.println("ID DE LA FOTO SUBIDA: " + fotoEscritorId);			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return fotoEscritorId;

	}
	
	//Para generar una cadena de n�meros aleatorios
	public static String generateRandomText() {
		 SecureRandom random = new SecureRandom();
		 String text = new BigInteger(130, random).toString(32);
		 
		 return text;
	}

	public void addLibro(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String titulo = ParamUtil.getString(request, "titulo");
		String genero = ParamUtil.getString(request, "genero");
		long idEscritor = ParamUtil.getLong(request, "escritor");
		String publicacion = ParamUtil.getString(request, "publicacion");

		Libro libro = _libroLocalService.fetchByTitulo(titulo);

		if (libro == null) {
			if ((genero != null && genero.length() < caracteres)
					&& (publicacion != null && publicacion.length() < caracteres)) {
				_libroLocalService.addLibro(themeDisplay.getScopeGroupId(), themeDisplay.getCompanyId(),
						themeDisplay.getUser().getUserId(), themeDisplay.getUser().getFullName(), titulo, publicacion,
						genero, idEscritor);
				response.setRenderParameter("mvcPath", "/listarLibros.jsp");
			}

		} else {
			SessionErrors.add(request, "libro-exist");
			SessionMessages.add(request,
					PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			response.setRenderParameter("mvcPath", "/addLibro.jsp");
		}

	}

	/**
	 * El m�todo recupera el Escritor a partir de su identificador, lo a�ade a
	 * la request y redirige a escritorEdit.jsp. Eso �ltimo se hace a�adiendo a
	 * la response el par�metro "mvcPath" �convenci�n de Liferay� con valor la
	 * ruta del JSP.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "displayEscritorEdition")
	public void displayEscritorEdition(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {
		String id = request.getParameter("idEscritor");
		Escritor escritor = EscritorLocalServiceUtil.getEscritor(Long.valueOf(id));

		request.setAttribute("escritor", escritor);
		response.setRenderParameter("mvcPath", "/escritorEdit.jsp");
	}

	/**
	 * Mismo m�todo, pero ahora con el objeto Libro.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "displayLibroEdition")
	public void displayLibroEdition(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {
		String idLibro = request.getParameter("idLibro");
		Libro libro = LibroLocalServiceUtil.getLibro(Long.valueOf(idLibro));

		request.setAttribute("libro", libro);
		response.setRenderParameter("mvcPath", "/libroEdit.jsp");
	}

	/**
	 * M�todo que responder� a la petici�n de ejecuci�n de la acci�n
	 * "editEscritor".
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "editEscritor")
	public void editEscritor(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {

		String id = request.getParameter("idEscritor");
		String nombre = request.getParameter("nombreEscritor");
		String nacionalidad = request.getParameter("nacionalidad");

		Escritor escritor = _escritorLocalService.fetchByNombre(nombre);

		if (escritor != null) {
			if (nacionalidad != null && nacionalidad.length() < caracteres) {
				_escritorLocalService.updateEscritor(Long.valueOf(id), nombre, nacionalidad);
				response.setRenderParameter("mvcPath", "/listarEscritores.jsp");
			}
		} else {
			SessionErrors.add(request, "escritor-exist");
			SessionMessages.add(request,
					PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			response.setRenderParameter("mvcPath", "/escritorEdit.jsp");
		}

	}

	/**
	 * M�todo que responder� a la petici�n de ejecuci�n de la acci�n
	 * "editLibro".
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "editLibro")
	public void editLibro(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {

		String idLibro = request.getParameter("idLibro");
		String titulo = request.getParameter("titulo");
		String publicacion = request.getParameter("publicacion");
		String genero = request.getParameter("genero");
		String idEscritor = request.getParameter("escritor");

		Libro libro = _libroLocalService.fetchByTitulo(titulo);

		if (libro != null) {
			if ((genero != null && genero.length() < caracteres)
					&& (publicacion != null && publicacion.length() < caracteres)) {
				_libroLocalService.updateLibro(Long.valueOf(idLibro), titulo, publicacion, genero,
						Long.valueOf(idEscritor));
				response.setRenderParameter("mvcPath", "/listarLibros.jsp");
			}
		} else {
			SessionErrors.add(request, "libro-exist");
			SessionMessages.add(request,
					PortalUtil.getPortletId(request) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			response.setRenderParameter("mvcPath", "/libroEdit.jsp");
		}

	}

	/**
	 * M�todo para eliminar escritores.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "deleteEscritor")
	public void deleteEscritor(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {
		String id = request.getParameter("idEscritor");

		EscritorLocalServiceUtil.deleteEscritor(Long.valueOf(id));
		response.setRenderParameter("mvcPath", "/listarEscritores.jsp");
	}

	/**
	 * M�todo para eliminar libros.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws PortletException
	 * @throws PortalException
	 */
	@ProcessAction(name = "deleteLibro")
	public void deleteLibro(ActionRequest request, ActionResponse response)
			throws IOException, PortletException, PortalException {
		String id = request.getParameter("idLibro");

		LibroLocalServiceUtil.deleteLibro(Long.valueOf(id));
		response.setRenderParameter("mvcPath", "/listarLibros.jsp");
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute("_escritorLocalService", _escritorLocalService);
		renderRequest.setAttribute("_libroLocalService", _libroLocalService);
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		String txtEscritores = ParamUtil.getString(resourceRequest, "downloadTxtButton");
		String csvEscritores = ParamUtil.getString(resourceRequest, "downloadCsvButton");
		String txtLibros = ParamUtil.getString(resourceRequest, "downloadTxtButtonLibros");
		String csvLibros = ParamUtil.getString(resourceRequest, "downloadCsvButtonLibros");

		if (txtEscritores.equals("Txt")) {
			String escritores = EscritorLocalServiceUtil.getEscritors(0, Integer.MAX_VALUE).toString();
			InputStream stream = new ByteArrayInputStream(escritores.getBytes(StandardCharsets.UTF_8));

			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, "escritores.txt", stream,
					ContentTypes.APPLICATION_TEXT);
		}

		if (txtLibros.equals("Txt")) {
			String libros = LibroLocalServiceUtil.getLibros(0, Integer.MAX_VALUE).toString();
			InputStream stream = new ByteArrayInputStream(libros.getBytes(StandardCharsets.UTF_8));

			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, "libros.txt", stream,
					ContentTypes.APPLICATION_TEXT);
		}

		if (csvEscritores.equals("Csv")) {
			StringBundler sb = new StringBundler();

			for (String columnName : columnNames1) {
				sb.append(getCSVFormattedValue(columnName));
				sb.append(CSV_SEPARATOR);
			}

			sb.setIndex(sb.index() - 1);
			sb.append(CharPool.NEW_LINE);

			List<Escritor> escritoresList = EscritorLocalServiceUtil.getEscritors(0,
					EscritorLocalServiceUtil.getEscritorsCount());

			for (Escritor escritor : escritoresList) {

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getEscritorId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getGroupId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getCompanyId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getUserId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getUserName())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getCreateDate())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getModifiedDate())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getNombre())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(escritor.getNacionalidad())));
				sb.append(CSV_SEPARATOR);

				sb.setIndex(sb.index() - 1);
				sb.append(CharPool.NEW_LINE);
			}

			String fileName1 = "escritores.csv";
			// mimetype
			byte[] bytes1 = sb.toString().getBytes();
			String contentType1 = ContentTypes.APPLICATION_TEXT;
			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, fileName1, bytes1, contentType1);
		}

		if (csvLibros.equals("Csv")) {
			StringBundler sb = new StringBundler();

			for (String columnName : columnNames2) {
				sb.append(getCSVFormattedValue(columnName));
				sb.append(CSV_SEPARATOR);
			}

			sb.setIndex(sb.index() - 1);
			sb.append(CharPool.NEW_LINE);

			List<Libro> librosList = LibroLocalServiceUtil.getLibros(0, LibroLocalServiceUtil.getLibrosCount());

			for (Libro libro : librosList) {

				sb.append(getCSVFormattedValue(String.valueOf(libro.getLibroId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getGroupId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getCompanyId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getUserId())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getUserName())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getCreateDate())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getModifiedDate())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getTitulo())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getGenero())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getPublicacion())));
				sb.append(CSV_SEPARATOR);

				sb.append(getCSVFormattedValue(String.valueOf(libro.getEscritorId())));
				sb.append(CSV_SEPARATOR);

				sb.setIndex(sb.index() - 1);
				sb.append(CharPool.NEW_LINE);
			}

			String fileName2 = "libros.csv";
			byte[] bytes2 = sb.toString().getBytes();
			String contentType2 = ContentTypes.APPLICATION_TEXT;
			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, fileName2, bytes2, contentType2);
		}
		super.serveResource(resourceRequest, resourceResponse);
	}

	public static String[] columnNames1 = { "escritorId", "groupId", "companyId", "userId", "userName", "createDate",
			"modifiedDate", "nombre", "nacionalidad" };

	public static String[] columnNames2 = { "libroId", "groupId", "companyId", "userId", "userName", "createDate",
			"modifiedDate", "titulo", "genero", "publicacion", "escritorId" };

	public static final String CSV_SEPARATOR = ",";

	protected String getCSVFormattedValue(String value) {
		StringBundler sb = new StringBundler(3);
		sb.append(CharPool.QUOTE);
		sb.append(StringUtil.replace(value, CharPool.QUOTE, StringPool.DOUBLE_QUOTE));
		sb.append(CharPool.QUOTE);
		return sb.toString();
	}

}