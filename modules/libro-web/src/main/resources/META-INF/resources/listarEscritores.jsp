<%@page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@ include file="./init.jsp"%>
<%@ include file="./view.jsp"%>

<style>
	.contenedor-imagenes {
	    position: relative;
	    width: 100%;
	    padding-top: 56.25%;
	    background-size: cover;
	    background-position: center;
	}

</style>

<br/>
<%	String orderByCol = ParamUtil.getString(request, "orderByCol", "nombre");
	String orderByType = ParamUtil.getString(request, "orderByType", "asc");
	OrderByComparator comparator = OrderByComparatorFactoryUtil.create("Escritores", orderByCol, "asc".equals(orderByType));	
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<liferay-portlet:param name="jspPage" value="/listarEscritores.jsp"/>
</liferay-portlet:renderURL>

<liferay-ui:header title="Listado escritores registrados en la BBDD:"></liferay-ui:header>

<liferay-ui:search-container iteratorURL="<%= iteratorURL %>" emptyResultsMessage="No has creado todavia ningun escritor." 
		delta="10" displayTerms="<%= new DisplayTerms(renderRequest) %>" orderByCol="<%= orderByCol %>" orderByType="<%= orderByType %>"
		orderByComparator="<%= comparator %>">
	<liferay-ui:search-container-results>
		<%
			results = _escritorLocalService.getEscritors(searchContainer.getStart(), searchContainer.getEnd());
			total = _escritorLocalService.getEscritorsCount();
			searchContainer.setTotal(total);
			searchContainer.setResults(results);
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="libro.model.Escritor" modelVar="escritor">
		<liferay-ui:search-container-column-text name="Nombre" property="nombre" />
		<liferay-ui:search-container-column-text name="Nacionalidad" property="nacionalidad" />			
		<liferay-ui:search-container-column-jsp name="Foto" path="/css-prueba.jsp" />					
		<liferay-ui:search-container-column-jsp name="Acciones" path="/escritorActionButtons.jsp" />
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator paginate="<%=true%>" />
</liferay-ui:search-container>


<liferay-portlet:resourceURL id="downloadTxt" var="downloadEscritoresUrl">
	<liferay-portlet:param name="downloadTxtButton" value="Txt"/>
</liferay-portlet:resourceURL>
<br/>
<aui:button name="downloadTxtButton" type="submit" value="Descargar lista de escritores.txt" onClick="<%=downloadEscritoresUrl%>" />
		

<liferay-portlet:resourceURL id="downloadCSV" var="downloadCSVfileUrl">
	<liferay-portlet:param name="downloadCsvButton" value="Csv"/>
</liferay-portlet:resourceURL>
<aui:button name="downloadCsvButton" type="submit" value="Descargar lista de escritores.csv"
	onClick="<%=downloadCSVfileUrl%>" />