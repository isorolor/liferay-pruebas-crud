<%@ include file="./init.jsp" %>

<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="libro.model.Escritor" %>

<%
    ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Escritor escritor = (Escritor) row.getObject();
%>

<portlet:renderURL var="displayEscritorEditionUrl">
    <portlet:param name="idEscritor" value="<%=String.valueOf(escritor.getEscritorId())%>"/>
    <portlet:param name="mvcPath" value="/escritorEdit.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="displayEscritorLibrosUrl">
    <portlet:param name="idEscritor" value="<%=String.valueOf(escritor.getEscritorId())%>"/>
    <portlet:param name="mvcPath" value="/escritorLibros.jsp"/>
</portlet:renderURL>

<portlet:actionURL name="deleteEscritor" var="deleteEscritorUrl">
    <portlet:param name="idEscritor" value="<%=String.valueOf(escritor.getEscritorId())%>"/>
</portlet:actionURL>

<liferay-ui:icon-menu>
    <liferay-ui:icon image="edit" message="Editar" url="<%=displayEscritorEditionUrl%>"/>
    <liferay-ui:icon image="delete" message="Eliminar" url="<%=deleteEscritorUrl%>"/>
    <liferay-ui:icon image="search" message="Libros asociados" url="<%=displayEscritorLibrosUrl%>"/>
</liferay-ui:icon-menu>