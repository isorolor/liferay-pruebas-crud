<%@ include file="./init.jsp"%>
<%@ include file="./view.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	    $("input:text").focus(function(){
	        $(this).css("background-color", "#DDDBF1");
	        $(this).css("font-size", "19px");
	    });
	    $("input:text").blur(function(){
	        $(this).css("background-color", "#F5F7F8");
	        $(this).css("font-size", "16px");
	    });
	});
</script>

<br/>
<liferay-ui:error key="escritor-exist" message="Escritor ya registrado" />
<liferay-ui:header title="A�ade un escritor a la BBDD"></liferay-ui:header>

<portlet:actionURL name="addEscritor" var="addEscritorUrl" />

<aui:form action="${addEscritorUrl}" enctype="multipart/form-data">
	<aui:input name="nombreEscritor" type="text" label="Escribe aqu� el nombre del escritor:">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="maxLength">75</aui:validator>
	</aui:input>
	<aui:input name="nacionalidad" type="text" label="Nacionalidad:">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="maxLength">75</aui:validator>
	</aui:input>
	<aui:input name="uploadedFile" type="file" label="Foto del tipo:">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="acceptFiles">'jpg,png,tif,gif'</aui:validator>
	</aui:input>
	<aui:button name="addEscritorButton" type="submit" value="Guardar escritor" />
</aui:form>