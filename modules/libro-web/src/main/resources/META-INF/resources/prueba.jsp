<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ include file="./init.jsp"%>
<%@ include file="./view.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	    $("input").focus(function(){
	        $(this).css("background-color", "#DDDBF1");
	        $(this).css("font-size", "19px");
	    });
	    $("input").blur(function(){
	        $(this).css("background-color", "#F5F7F8");
	        $(this).css("font-size", "16px");
	    });
	});
</script>

<%	long escritorId = ParamUtil.getLong(request, "escritorId");
	boolean isEdit = escritorId != 0;
	Escritor escritor = null;
	if (isEdit) {
		escritor = _escritorLocalService.getEscritor(escritorId);
	}
%>

<liferay-portlet:renderURL var="volverListado">
	<liferay-portlet:param name="mvcPath" value="/addEditMismoJSP.jsp"/>
</liferay-portlet:renderURL>

<%	if (isEdit) { %>
	<portlet:actionURL name="editEscritor" var="editEscritorUrl" />

	<h1>Editar datos:</h1>
	<aui:form action="${editEscritorUrl}">
	    <aui:input name="nombreEscritor" label="Modifica aqu� el nombre del escritor:" value="<%=escritor.getNombre() %>"/>
	    <aui:input name="nacionalidad" label="Modifica aqu� su nacionalidad:" value="<%=escritor.getNacionalidad() %>"/>
	    <aui:input name="idEscritor" type="hidden" value="<%=String.valueOf(escritor.getEscritorId()) %>"/>
	    <aui:button name="editEscritorButton" type="submit" value="Editar escritor"/>
	    <a href="${volverListado}" class="btn btn-primary">
			<liferay-ui:message key="Volver" />
		</a>
	</aui:form>	

<%	} else { %>
	<portlet:actionURL name="addEscritor" var="addEscritorUrl" />

	<h1>A�adir nuevo:</h1>
	<aui:form action="${addEscritorUrl}">
		<aui:input name="nombreEscritor" type="text" label="Escribe aqu� el nombre del escritor:">
			<aui:validator name="required"></aui:validator>
			<aui:validator name="maxLength">75</aui:validator>
		</aui:input>
		<aui:input name="nacionalidad" type="text" label="Nacionalidad:">
			<aui:validator name="required"></aui:validator>
			<aui:validator name="maxLength">75</aui:validator>
		</aui:input>
			<aui:input name="uploadedFile" type="file" label="Foto del tipo:">
			<aui:validator name="required"></aui:validator>
			<aui:validator name="acceptFiles">'jpg,png,tif,gif'</aui:validator>
		</aui:input>
		<aui:button name="addEscritorButton" type="submit" value="Guardar escritor" />
		<a href="${volverListado}" class="btn btn-primary">
			<liferay-ui:message key="Volver" />
		</a>
	</aui:form>	
	
<%	} %>