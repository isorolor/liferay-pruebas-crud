<%@ page import="libro.model.Libro"%>
<%@ page import="libro.service.LibroLocalService"%>
<%@ page import="java.util.List"%>
<%@ page import="libro.model.Escritor"%>
<%@ page import="libro.service.EscritorLocalService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@ page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.repository.model.FileEntry" %>

<%@ include file="./init.jsp"%>

<%
	EscritorLocalService _escritorLocalService = (EscritorLocalService) request.getAttribute("_escritorLocalService");
	LibroLocalService _libroLocalService = (LibroLocalService) request.getAttribute("_libroLocalService");
%>

<liferay-portlet:renderURL var="addEscritorURL">
	<liferay-portlet:param name="jspPage" value="/addEscritor.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="listarEscritoresURL">
	<liferay-portlet:param name="jspPage" value="/listarEscritores.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="addLibroURL">
	<liferay-portlet:param name="jspPage" value="/addLibro.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="listarLibrosURL">
	<liferay-portlet:param name="jspPage" value="/listarLibros.jsp" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="addEditEscritorMismoFormURL">
	<liferay-portlet:param name="jspPage" value="/addEditMismoJSP.jsp" />
</liferay-portlet:renderURL>








<h2>CRUD Escritores - Libros</h2>

<div class="menu-navegacion" style="font-size: 17px;">	
	<liferay-ui:icon-list>
		<liferay-ui:icon iconCssClass="icon-edit" label="<%= true %>" message="Añadir escritor" url="${addEscritorURL}"/>
		<liferay-ui:icon iconCssClass="icon-film" label="<%= true %>" message="Listado escritores" url="${listarEscritoresURL}"/>
		<liferay-ui:icon iconCssClass="icon-edit" label="<%= true %>" message="Añadir libro" url="${addLibroURL}"/>
		<liferay-ui:icon iconCssClass="icon-film" label="<%= true %>" message="Listado libros" url="${listarLibrosURL}"/>
		<liferay-ui:icon iconCssClass="icon-edit" label="<%= true %>" message="Añadir/Editar desde el mismo formulario" url="${addEditEscritorMismoFormURL}"/>
	</liferay-ui:icon-list>
</div>


<!-- 
<div class="container">  
  <div id="myCarousel" class="carousel slide" data-ride="carousel"> 

    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner">
      <div class="item active">      
      <%	FileEntry fl1 = DLAppLocalServiceUtil.getFileEntry(39045);
      		String rutaImagen1 = "/documents/" + fl1.getRepositoryId() + "/" + fl1.getFolderId() + "/" + fl1.getTitle(); %>
        <img src="<%= rutaImagen1 %>" class="tales" style="width:100%; height: 200px;">
      </div>

      <div class="item">
      <%	FileEntry fl2 = DLAppLocalServiceUtil.getFileEntry(39060);
      		String rutaImagen2 = "/documents/" + fl2.getRepositoryId() + "/" + fl2.getFolderId() + "/" + fl2.getTitle(); %>
        <img src="<%= rutaImagen2 %>" class="tales" style="width:100%; height: 200px;">
      </div>
    
      <div class="item">
      <%	FileEntry fl3 = DLAppLocalServiceUtil.getFileEntry(39071);
      		String rutaImagen3 = "/documents/" + fl3.getRepositoryId() + "/" + fl3.getFolderId() + "/" + fl3.getTitle(); %>
        <img src="<%= rutaImagen3 %>" class="tales" style="width:100%; height: 200px;">
      </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>  
-->