<%@ page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.repository.model.FileEntry" %>
<%@ include file="./init.jsp"%>

<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="libro.model.Escritor" %>

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Escritor escritor = (Escritor) row.getObject();
%>

<%
	long imgId = escritor.getFotoEscritorId();
	if (imgId == 0) {

	} else {
		FileEntry fl = DLAppLocalServiceUtil.getFileEntry(imgId);
		String rutaImagen = "/documents/" + fl.getRepositoryId() + "/" + fl.getFolderId() + "/" + fl.getTitle();
%>
<div class="contenedor-imagenes" style="background-image: url('<%= rutaImagen %>');" >
</div>	
<%
	}
%>
