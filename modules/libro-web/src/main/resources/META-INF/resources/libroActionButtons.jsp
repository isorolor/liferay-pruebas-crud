<%@ include file="./init.jsp" %>

<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="libro.model.Libro" %>

<%
    ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    Libro libro = (Libro) row.getObject();
%>

<portlet:renderURL var="displayLibroEditionUrl">
    <portlet:param name="idLibro" value="<%=String.valueOf(libro.getLibroId())%>"/>
    <portlet:param name="mvcPath" value="/libroEdit.jsp"/>
</portlet:renderURL>

<portlet:actionURL name="deleteLibro" var="deleteLibroUrl">
    <portlet:param name="idLibro" value="<%=String.valueOf(libro.getLibroId())%>"/>
</portlet:actionURL>

<liferay-ui:icon-menu>
    <liferay-ui:icon image="edit" message="Editar" url="<%=displayLibroEditionUrl%>"/>
    <liferay-ui:icon image="delete" message="Eliminar" url="<%=deleteLibroUrl%>"/>
</liferay-ui:icon-menu>