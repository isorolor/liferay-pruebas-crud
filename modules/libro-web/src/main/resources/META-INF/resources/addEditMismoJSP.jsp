<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.DisplayTerms"%>
<%@ include file="./init.jsp"%>
<%@ include file="./view.jsp"%>

<style>
	.contenedor-imagenes {
	    position: relative;
	    width: 100%;
	    padding-top: 56.25%;
	    background-size: cover;
	    background-position: center;
	}

</style>

<br/>
<%	String orderByCol = ParamUtil.getString(request, "orderByCol", "nombre");
	String orderByType = ParamUtil.getString(request, "orderByType", "asc");
	OrderByComparator comparator = OrderByComparatorFactoryUtil.create("Escritores", orderByCol, "asc".equals(orderByType));	
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<liferay-portlet:param name="jspPage" value="/addEditMismoJSP.jsp"/>
</liferay-portlet:renderURL>

<liferay-ui:header title="En funci�n de si le pasamos la ID del Escritor (o no) editar� el existente o a�adir� uno nuevo!"></liferay-ui:header>

<liferay-ui:search-container iteratorURL="<%= iteratorURL %>" emptyResultsMessage="No has creado todavia ningun escritor." 
		delta="10" displayTerms="<%= new DisplayTerms(renderRequest) %>" orderByCol="<%= orderByCol %>" orderByType="<%= orderByType %>"
		orderByComparator="<%= comparator %>">
	<liferay-ui:search-container-results>
		<%
			results = _escritorLocalService.getEscritors(searchContainer.getStart(), searchContainer.getEnd());
			total = _escritorLocalService.getEscritorsCount();
			searchContainer.setTotal(total);
			searchContainer.setResults(results);
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="libro.model.Escritor" modelVar="escritor">
		<liferay-ui:search-container-column-text name="Nombre" property="nombre" />
		<liferay-ui:search-container-column-text name="Nacionalidad" property="nacionalidad" />
		<liferay-ui:search-container-column-jsp name="Foto" path="/css-prueba.jsp" />	
		
		<liferay-ui:search-container-column-text name="actions">
			<liferay-ui:icon-menu>
				<liferay-portlet:renderURL var="editEscritorURL">
					<liferay-portlet:param name="mvcPath" value="/prueba.jsp" />
					<liferay-portlet:param name="escritorId" value="<%= String.valueOf(escritor.getEscritorId()) %>" />
				</liferay-portlet:renderURL>
				<liferay-ui:icon image="edit" message="Editar" label="edit-escritor" url="<%= editEscritorURL %>" />
				<portlet:actionURL name="deleteEscritor" var="deleteEscritorUrl">
				    <portlet:param name="idEscritor" value="<%=String.valueOf(escritor.getEscritorId())%>"/>
				</portlet:actionURL>
				<liferay-ui:icon image="delete" message="Eliminar" url="<%=deleteEscritorUrl%>"/>
			</liferay-ui:icon-menu>
		</liferay-ui:search-container-column-text>		
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator paginate="<%=true%>" />
</liferay-ui:search-container>

<liferay-portlet:renderURL var="addEscritorURL">
		<liferay-portlet:param name="mvcPath" value="/prueba.jsp" />
</liferay-portlet:renderURL>

<a href="${addEscritorURL}" class="btn btn-primary">
	<liferay-ui:message key="A�adir" />
</a>