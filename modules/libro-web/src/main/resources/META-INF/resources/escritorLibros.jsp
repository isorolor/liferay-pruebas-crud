<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.dao.search.DisplayTerms" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ include file="./init.jsp" %>
<%@ include file="./view.jsp" %>

<%
	String orderByCol = ParamUtil.getString(request, "orderByCol", "titulo");
	String orderByType = ParamUtil.getString(request, "orderByType", "asc");
	OrderByComparator comparator = OrderByComparatorFactoryUtil.create("Libros", orderByCol,
			"asc".equals(orderByType));

	long escritorId = ParamUtil.getLong(request, "idEscritor", 0);
	Escritor escritor = _escritorLocalService.fetchEscritor(escritorId);
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<liferay-portlet:param name="jspPage" value="/escritorLibros.jsp" />
</liferay-portlet:renderURL>

<c:if test="<%=escritor != null%>">
	<br />
	<liferay-ui:header title="<%=escritor.getNombre()%>"></liferay-ui:header>

	<liferay-ui:search-container iteratorURL="<%= iteratorURL %>"
		emptyResultsMessage="Todavía no hay libros registrados." delta="10"
		displayTerms="<%=new DisplayTerms(renderRequest)%>"
		orderByCol="<%= orderByCol %>" orderByType="<%=orderByType%>"
		orderByComparator="<%= comparator %>">
		<liferay-ui:search-container-results>
			<%
				results = _libroLocalService.findByEscritorId(escritorId);
							total = _libroLocalService.getLibrosCount();
							searchContainer.setTotal(total);
							searchContainer.setResults(results);
			%>
		</liferay-ui:search-container-results>

		<liferay-ui:search-container-row className="libro.model.Libro"
			modelVar="libro">
			<liferay-ui:search-container-column-text name="Título"
				property="titulo" />
			<liferay-ui:search-container-column-text name="Publicación"
				property="publicacion" />
			<liferay-ui:search-container-column-text name="Género"
				property="genero" />
		</liferay-ui:search-container-row>


		<liferay-ui:search-iterator paginate="<%=true%>" />
	</liferay-ui:search-container>
</c:if>