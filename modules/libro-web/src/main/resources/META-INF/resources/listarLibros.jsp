<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.dao.search.DisplayTerms" %>

<%@ include file="./init.jsp" %>
<%@ include file="./view.jsp" %>

<br/>
<%	String orderByCol = ParamUtil.getString(request, "orderByCol", "titulo");
	String orderByType = ParamUtil.getString(request, "orderByType", "asc");
	OrderByComparator comparator = OrderByComparatorFactoryUtil.create("Libros", orderByCol, "asc".equals(orderByType));	
%>

<liferay-portlet:renderURL varImpl="iteratorURL">
	<liferay-portlet:param name="jspPage" value="/listarLibros.jsp"/>
</liferay-portlet:renderURL>

<liferay-ui:header title="Libros registrados en la BBDD:"></liferay-ui:header>

<liferay-ui:search-container iteratorURL="<%= iteratorURL %>" emptyResultsMessage="No has a�adido todav�a ning�n libro." delta="10"
		displayTerms="<%= new DisplayTerms(renderRequest) %>" orderByCol="<%= orderByCol %>" orderByType="<%= orderByType %>"
		orderByComparator="<%= comparator %>">
	<liferay-ui:search-container-results>
		<%
			results = _libroLocalService.getLibros(searchContainer.getStart(), searchContainer.getEnd());
			total = _libroLocalService.getLibrosCount();
			searchContainer.setTotal(total);
			searchContainer.setResults(results);			
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="libro.model.Libro" modelVar="libro">
		<liferay-ui:search-container-column-text name="T�tulo" property="titulo" />
		<liferay-ui:search-container-column-text name="Publicaci�n" property="publicacion" />
		<liferay-ui:search-container-column-text name="G�nero" property="genero" />
		<%
		Escritor escritor = _escritorLocalService.fetchEscritor(libro.getEscritorId());		
		String nombreEscritor = "No encontrado";
		if (escritor != null) {
			nombreEscritor = escritor.getNombre();
		}
		%>
		<liferay-ui:search-container-column-text name="Escritor" value="<%= nombreEscritor %>" />
		<liferay-ui:search-container-column-jsp name="Acciones" path="/libroActionButtons.jsp" />	
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator paginate="<%=true%>" />
</liferay-ui:search-container>


<liferay-portlet:resourceURL id="downloadTxt" var="downloadLibrosUrl">
	<liferay-portlet:param name="downloadTxtButtonLibros" value="Txt"/>
</liferay-portlet:resourceURL>
<br/>
<aui:button name="downloadTxtButtonLibros" type="submit" value="Descargar lista de libros.txt"
	onClick="<%=downloadLibrosUrl%>" />
		

<liferay-portlet:resourceURL id="downloadCSV" var="downloadCSVLibrosUrl">
	<liferay-portlet:param name="downloadCsvButtonLibros" value="Csv"/>
</liferay-portlet:resourceURL>
<aui:button name="downloadCsvButtonLibros" type="submit" value="Descargar lista de libros.csv"
	onClick="<%=downloadCSVLibrosUrl%>" />
	