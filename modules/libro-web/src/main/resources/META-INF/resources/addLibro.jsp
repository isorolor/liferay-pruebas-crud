<%@ include file="./init.jsp"%>
<%@ include file="./view.jsp"%>

<%
	List<Escritor> listadoEscritores = _escritorLocalService.getEscritors(-1, -1);
%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	    $("input").focus(function(){
	        $(this).css("background-color", "#DDDBF1");
	        $(this).css("font-size", "19px");
	    });
	    $("input").blur(function(){
	        $(this).css("background-color", "#F5F7F8");
	        $(this).css("font-size", "16px");
	    });
	});
</script>

<br />
<liferay-ui:error key="libro-exist" message="Libro ya registrado" />
<liferay-ui:header title="A�ade un libro a la BBDD"></liferay-ui:header>

<portlet:actionURL name="addLibro" var="addLibroUrl" />

<aui:form action="${addLibroUrl}">
	<aui:input name="titulo" type="text"
		label="Escribe aqu� el nombre del libro:">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="maxLength">75</aui:validator>
	</aui:input>
	<aui:input name="genero" type="text" label="G�nero del libro:">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="maxLength">75</aui:validator>
	</aui:input>

	<aui:select name="escritor" id="escritorSelect" label="Escritor">
		<aui:validator name="required"></aui:validator>
		<% for (Escritor escritor : listadoEscritores) { %>
		<aui:option value="<%=escritor.getEscritorId()%>"
			label="<%=escritor.getNombre()%>"></aui:option>
		<%
			}
		%>
	</aui:select>

	<aui:input name="publicacion" id="publicacion" type="text" cssClass="datepicker" label="Fecha de publicaci�n: (dd/mm/yyyy)">
		<aui:validator name="required"></aui:validator>
		<aui:validator name="maxLength">75</aui:validator>
	</aui:input>

	<aui:script>
			YUI().use(
			  'aui-datepicker',
			  function(Y) {
			    new Y.DatePicker(
			      {
			        trigger: '.datepicker',
			        mask: '%d/%m/%Y',
			        popover: {
			          zIndex: 1
			        },
			        on: {
			          selectionChange: function(event) {
			            console.log(event.newSelection)
					 	}
			       	}
			      }
			    );
			  }
			);

	</aui:script>
	
	<aui:button name="addLibroButton" type="submit" value="Guardar libro" />
</aui:form>