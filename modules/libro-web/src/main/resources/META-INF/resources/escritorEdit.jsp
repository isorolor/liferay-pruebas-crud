<%@page import="libro.service.EscritorLocalService"%>
<%@page import="libro.model.Escritor"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
 
<%@ include file="./init.jsp" %>
<%@ include file="./view.jsp" %>

<%
long escritorId = ParamUtil.getLong(request, "idEscritor", 0);
Escritor escritor = _escritorLocalService.fetchEscritor(escritorId);
%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	    $("input").focus(function(){
	        $(this).css("background-color", "#DDDBF1");
	        $(this).css("font-size", "19px");
	    });
	    $("input").blur(function(){
	        $(this).css("background-color", "#F5F7F8");
	        $(this).css("font-size", "16px");
	    });
	});
</script>

<liferay-ui:error key="escritor-exist" message="Escritor ya registrado" />
<c:if test="<%= escritor != null %>">
<portlet:actionURL name="editEscritor" var="editEscritorUrl" />

<aui:form action="${editEscritorUrl}">
    <aui:input name="nombreEscritor" label="Modifica aquí el nombre del escritor:" value="<%=escritor.getNombre() %>"/>
    <aui:input name="nacionalidad" label="Modifica aquí su nacionalidad:" value="<%=escritor.getNacionalidad() %>"/>
    <aui:input name="idEscritor" type="hidden" value="<%=String.valueOf(escritor.getEscritorId()) %>"/>
    <aui:button name="editEscritorButton" type="submit" value="Editar escritor"/>
</aui:form>
</c:if>