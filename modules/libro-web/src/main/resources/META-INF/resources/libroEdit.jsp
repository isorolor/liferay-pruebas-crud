<%@page import="libro.service.LibroLocalService"%>
<%@page import="libro.model.Libro"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
 
<%@ include file="./init.jsp" %>
<%@ include file="./view.jsp" %>

<%
	long libroId = ParamUtil.getLong(request, "idLibro", 0);
	Libro libro = _libroLocalService.fetchLibro(libroId);
	List<Escritor> listadoEscritores = _escritorLocalService.getEscritors(-1, -1);
%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
	    $("input").focus(function(){
	        $(this).css("background-color", "#DDDBF1");
	        $(this).css("font-size", "19px");
	    });
	    $("input").blur(function(){
	        $(this).css("background-color", "#F5F7F8");
	        $(this).css("font-size", "16px");
	    });
	});
</script>

<liferay-ui:error key="libro-exist" message="Libro ya registrado" />
<c:if test="<%= libro != null %>">
<portlet:actionURL name="editLibro" var="editLibroUrl">
</portlet:actionURL>

<aui:form action="${editLibroUrl}">
    <aui:input name="titulo" label="Modifica aquí el titulo del libro:" value="<%= libro.getTitulo() %>"/>
    <aui:input name="publicacion" cssClass="datepicker" label="Modifica su fecha de publicación (dd-mm-yyyy):" value="<%= libro.getPublicacion() %>"/>
    <aui:script>
			YUI().use(
			  'aui-datepicker',
			  function(Y) {
			    new Y.DatePicker(
			      {
			        trigger: '.datepicker',
			        mask: '%d/%m/%Y',
			        popover: {
			          zIndex: 1
			        },
			        on: {
			          selectionChange: function(event) {
			            console.log(event.newSelection)
					 	}
			       	}
			      }
			    );
			  }
			);

	</aui:script>
    <aui:input name="genero" label="Modifica el género:" value="<%= libro.getGenero() %>"/>
    
   	<aui:select name="escritor" id="escritorSelect" label="Modifica su escritor:" value="<%= libro.getEscritorId() %>">
			<% for (Escritor escritor : listadoEscritores) { %>
				<aui:option value="<%= escritor.getEscritorId() %>" label="<%= escritor.getNombre() %>"/>
			<% } %>			
	</aui:select>
		
    <aui:input name="idLibro" type="hidden" value="<%=String.valueOf(libro.getLibroId()) %>"/>
    <aui:button name="editLibroButton" type="submit" value="Editar libro"/>
</aui:form>
</c:if>