package libro.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.OrderByComparator;

import libro.exception.NoSuchEscritorException;
import libro.model.Escritor;
import libro.model.impl.EscritorImpl;
import libro.service.base.EscritorLocalServiceBaseImpl;
import libro.service.persistence.EscritorUtil;

public class EscritorLocalServiceImpl extends EscritorLocalServiceBaseImpl {
	
	@Override
	public void addEscritor(long groupId, long companyId, long userId, String userName, String nombre) {
        final Escritor escritor = new EscritorImpl();
        escritor.setEscritorId(counterLocalService.increment());
        escritor.setGroupId(groupId);
        escritor.setCompanyId(companyId);
        escritor.setUserId(userId);
        escritor.setUserName(userName);
        escritor.setNombre(nombre);
        
        addEscritor(escritor);
    }
	
	@Override
	public void addEscritor(long groupId, long companyId, long userId, String userName, String nombre, long fotoEscritorId) {
        final Escritor escritor = new EscritorImpl();
        escritor.setEscritorId(counterLocalService.increment());
        escritor.setGroupId(groupId);
        escritor.setCompanyId(companyId);
        escritor.setUserId(userId);
        escritor.setUserName(userName);
        escritor.setNombre(nombre);
        escritor.setFotoEscritorId(fotoEscritorId);
        
        addEscritor(escritor);
    }
	
	@Override
	public void addEscritor(long groupId, long companyId, long userId, String userName, String nombre, String nacionalidad) {
		final Escritor escritor = new EscritorImpl();
        escritor.setEscritorId(counterLocalService.increment());
        escritor.setGroupId(groupId);
        escritor.setCompanyId(companyId);
        escritor.setUserId(userId);
        escritor.setUserName(userName);
        escritor.setNombre(nombre);
        escritor.setNacionalidad(nacionalidad);
        
        addEscritor(escritor);
	}
	
	@Override
	public void addEscritor2(long groupId, long companyId, long userId, String userName, String nombre, String nacionalidad, long fotoEscritorId) {
		final Escritor escritor = new EscritorImpl();
        escritor.setEscritorId(counterLocalService.increment());
        escritor.setGroupId(groupId);
        escritor.setCompanyId(companyId);
        escritor.setUserId(userId);
        escritor.setUserName(userName);
        escritor.setNombre(nombre);
        escritor.setNacionalidad(nacionalidad);
        escritor.setFotoEscritorId(fotoEscritorId);
        
        addEscritor(escritor);
	}
	
	public void updateEscritor(long id, String nombre) throws PortalException {
	    final Escritor escritor = getEscritor(id);
	    escritor.setNombre(nombre);
	 
	    updateEscritor(escritor);
	}
	
	public void updateEscritor(long id, String nombre, String nacionalidad) throws PortalException {
	    final Escritor escritor = getEscritor(id);
	    escritor.setNombre(nombre);
	    escritor.setNacionalidad(nacionalidad);
	 
	    updateEscritor(escritor);
	}
	
	public List<Escritor> find(String nacionalidad) {
		return EscritorUtil.findByNacionalidad(nacionalidad);
	}
	
	public Escritor findByNombre(String nombre) throws NoSuchEscritorException {
		return escritorPersistence.findByNombre(nombre);
	}
	
	public Escritor fetchByNombre(String nombre) {
		return escritorPersistence.fetchByNombre(nombre);
	}

	@Override
	public void addLibroEscritor(long libroId, Escritor escritor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLibroEscritor(long libroId, long escritorId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLibroEscritors(long libroId, List<Escritor> escritors) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLibroEscritors(long libroId, long[] escritorIds) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearLibroEscritors(long libroId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteLibroEscritor(long libroId, Escritor escritor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteLibroEscritor(long libroId, long escritorId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteLibroEscritors(long libroId, List<Escritor> escritors) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteLibroEscritors(long libroId, long[] escritorIds) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Escritor> getLibroEscritors(long libroId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Escritor> getLibroEscritors(long libroId, int start, int end) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Escritor> getLibroEscritors(long libroId, int start, int end,
			OrderByComparator<Escritor> orderByComparator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getLibroEscritorsCount(long libroId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasLibroEscritor(long libroId, long escritorId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasLibroEscritors(long libroId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setLibroEscritors(long libroId, long[] escritorIds) {
		// TODO Auto-generated method stub
		
	}
	
	
}