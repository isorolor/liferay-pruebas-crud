package libro.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import libro.exception.NoSuchLibroException;
import libro.model.Libro;
import libro.model.impl.LibroImpl;
import libro.service.base.LibroLocalServiceBaseImpl;

@ProviderType
public class LibroLocalServiceImpl extends LibroLocalServiceBaseImpl {
	
	public void addLibro(long groupId, long companyId, long userId, String userName, String titulo, 
			String publicacion, String genero, long escritorId) {
		Libro libro = new LibroImpl();
		
		libro.setLibroId(counterLocalService.increment());
        libro.setGroupId(groupId);
        libro.setCompanyId(companyId);
        libro.setUserId(userId);
        libro.setUserName(userName);
        
        libro.setPublicacion(publicacion);
        libro.setTitulo(titulo);
        libro.setGenero(genero);
        libro.setEscritorId(escritorId);
 
        addLibro(libro);
	}

	public void updateLibro(long id, String titulo, String publicacion, String genero, long escritorId) throws PortalException {
	    final Libro libro = getLibro(id);
	    libro.setTitulo(titulo);
	    libro.setPublicacion(publicacion);
	    libro.setGenero(genero);
	    libro.setEscritorId(escritorId);
	 
	    updateLibro(libro);
	}

	public Libro findByTitulo(String titulo) throws NoSuchLibroException {
		return libroPersistence.findByTitulo(titulo);
	}
	
	public Libro fetchByTitulo(String titulo) {
		return libroPersistence.fetchByTitulo(titulo);
	}
	
	public List<Libro> findByEscritorId(long escritorId) {
		return libroPersistence.findByEscritorId(escritorId);
	}
	

}