<p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
    Link with href
  </a>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
    Button with data-target
  </button>
</p>
<div class="collapse" id="collapseExample2">
  <div class="card card-body">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
  </div>
</div>

<div class="navvv">
	<a class="botonMenu" data-toggle="collapse" href="#collapseExample"
		role="button" aria-expanded="false" aria-controls="collapseExample">
		<img src="${themeDisplay.getPathThemeImages()}/ico-menu.png"
		width:20px height:20px> MENU</a> 
	<a class="botonCentral"><img src="${themeDisplay.getPathThemeImages()}/navarrasm.png"></a> 
	<a class="botonDerecha1" data-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">
		<img src="${themeDisplay.getPathThemeImages()}/ico-atendemos.png" width:20px height:20px> TE ATENDEMOS |</a>
	<a class="botonDerecha2" data-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">
		<img src="${themeDisplay.getPathThemeImages()}/ico-idioma.png" width:20px height:20px> IDIOMAS |</a>
</div>
<ul class="dropdowm-menu">
	<div class="collapse" id="collapseExample">
		<div class="card-body">
			<a href="#">MENU PRINCIPAL</a>
		</div>
		<div class="card-body">
			<a href="#">NAVARRA</a>
		</div>
		<div class="card-body">
			<a href="#">TRAMITES</a>
		</div>
		<div class="card-body">
			<a href="#">TEMAS</a>
		</div>
		<div class="card-body">
			<a href="#">GOBIERNO</a>
		</div>
		<div class="card-body">
			<a href="#">ACTUALIDAD</a>
		</div>
	</div>
</ul>