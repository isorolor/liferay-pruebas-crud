<div id="ctl00_divPie" class="container">
	<div class="row pie1" style="padding-top: 20px;">
		<div class="col-md-6 col-sm-4 logo">
			<a id="ctl00_enlaceGobiernoPie" title="Gobierno de Navarra"
				href="#">
				<img id="ctl00_imgGobiernoPie" src="${themeDisplay.getPathThemeImages()}/gobierno-navarra.png" alt="Gobierno de Navarra"
				style="height: 39px; width: 170px; border-width: 0px;"></a>
		</div>
		<div class="col-md-4 col-sm-6">
			<ul>
				<li><a href="/home_es/Indices/Sugerencias/default.htm"
					accesskey="c"> Contacto </a></li>

				<li><a href="/home_es/Aviso/accesibilidad.htm" accesskey="2">
						Accesibilidad </a></li>
				<li><a href="/home_es/Aviso/avisoLegal.htm"> Aviso legal </a></li>
			</ul>
		</div>
		<div class="col-md-2 col-sm-6 social">
			<ul>
				<li><a id="ctl00_lnkRedesPie1" title="twitter" class="facebook"
					href="https://www.twitter.com/navarra"><img src="${themeDisplay.getPathThemeImages()}/ico_twitter.png">
					</a>		
				</li>
				<li><a id="ctl00_lnkRedesPie2" title="facebook" class="facebook"
					href="https://www.twitter.com/navarra"><img src="${themeDisplay.getPathThemeImages()}/ico_fb.png">
					</a>		
				</li>
				<li><a id="ctl00_lnkRedesPie3" title="youtube" class="facebook"
					href="https://www.twitter.com/navarra"><img src="${themeDisplay.getPathThemeImages()}/iconyoutube.png">
					</a>		
				</li>
				<li><a id="ctl00_lnkRedesPie4" class="facebook"
					style="background-image: url(''); background-position: center center; background-repeat: no-repeat; overflow: hidden; text-indent: -9999px;"></a>
				</li>
			</ul>
		</div>
	</div>
</div>