<div id="ctl00_divPie" class="container">
	<div class="row pie1" style="padding-top: 20px;">
		<div class="col-md-6 col-sm-4 logo">
			<a id="ctl00_enlaceGobiernoPie" title="Gobierno de Navarra" href="#">
				<img id="ctl00_imgGobiernoPie"
				src="${themeDisplay.getPathThemeImages()}/gobierno-navarra.png"
				alt="Gobierno de Navarra"
				style="height: 39px; width: 170px; border-width: 0px;">
			</a>
		</div>
		<div class="col-md-4 col-sm-6">
			<ul>
				<li><a href="#" accesskey="c"> Contacto </a></li>

				<li><a href="#" accesskey="2"> Accesibilidad </a></li>
				<li><a href="#"> Aviso legal </a></li>
			</ul>
		</div>
		<div class="col-md-2 col-sm-6 social">
			<ul>
				<li><a id="ctl00_lnkRedesPie1" title="twitter" class="facebook"
					href="https://www.twitter.com/navarra"><img
						src="${themeDisplay.getPathThemeImages()}/ico_twitter.png">
				</a></li>
				<li><a id="ctl00_lnkRedesPie2" title="facebook"
					class="facebook" href="https://www.twitter.com/navarra"><img
						src="${themeDisplay.getPathThemeImages()}/ico_fb.png"> </a></li>
				<li><a id="ctl00_lnkRedesPie3" title="youtube" class="facebook"
					href="https://www.twitter.com/navarra"><img
						src="${themeDisplay.getPathThemeImages()}/iconyoutube.png">
				</a></li>
				<li><a id="ctl00_lnkRedesPie4" class="facebook"
					style="background-image: url(''); background-position: center center; background-repeat: no-repeat; overflow: hidden; text-indent: -9999px;"></a>
				</li>
			</ul>
		</div>
	</div>
	
	<!-- A partir de aquí metemos el visor de contenido -->
	
	<div class="row pie2">
		<div class="col-sm-2 col">
			<span class="titulo"> <a id="ctl00_lnkPieZona1Titulo"
				href="/home_es/Navarra/">Navarra</a>
			</span>
			<p>
				<a href="/home_es/Navarra/Asi+es+Navarra/">Conoce la Comunidad</a>
			</p>
			<p>
				<a href="/home_es/Navarra/Instituciones/">Instituciones</a>
			</p>
			<p>
				<a href="/home_es/Navarra/Asi+es+Navarra/Navarra+en+cifras/">Indicadores
					socioeconómicos</a>
			</p>
			<p>
				<a href="http://www.turismo.navarra.es">Turismo</a>
			</p>
			<p>
				<a href="/home_es/Navarra/Instituciones/Elecciones+2015/">Elecciones</a>
			</p>
			<p>
				<a href="/home_es/Navarra/Derecho+navarro/">Derecho navarro</a>
			</p>
			<p>
				<a href="/home_es/Navarra/272+Municipios/">Municipios</a>
			</p>
		</div>

		<div class="col-sm-2 col">
			<span class="titulo"> <a id="ctl00_lnkPieZona2Titulo"
				href="/home_es/Servicios/buscador/0/0/0/-1/0/-/0/0/-">Trámites</a>
			</span>
			<p>
				<a href="/home_es/Servicios/ficha/1718/Registro-General-Electronico">Registro
					general electrónico</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/12/-/0/0/-">Ayudas
					y subvenciones</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/29/-/0/0/-">Empleo
					público</a>
			</p>
			<p>
				<a href="/home_es/Servicios/PortalContratacion/">Contratación
					pública</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/17/-/0/0/-">Impuestos
					y tasas</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/11/-/0/0/-">Carnés
					y permisos</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/13/-/0/0/-">Certificados
					y registros</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/39/-/0/0/-">Comunicaciones
					a la Administración</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/38/-/0/0/-">Formación</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/18/-/0/0/-">Prestaciones
					sociales</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/40/-/0/0/-">Denuncias
					y reclamaciones</a>
			</p>
			<p>
				<a href="/home_es/Servicios/lo-mas-nuevo/">Lo más nuevo</a>
			</p>
			<p>
				<a href="/home_es/Servicios/ultimos-dias/">Últimos días</a>
			</p>
			<p>
				<a href="/home_es/Servicios/buscador/1/0/0/-1/-1/-/0/0/-">Todos</a>
			</p>
		</div>

		<div class="col-sm-2 col">
			<span class="titulo"> <a id="ctl00_lnkPieZona3Titulo"
				href="/home_es/Temas/">Temas</a>
			</span>
			<p>
				<a href="/home_es/Temas/Asuntos+sociales/default.htm">Asuntos
					sociales</a>
			</p>
			<p>
				<a href="/home_es/Temas/Consumo/">Consumo</a>
			</p>
			<p>
				<a href="/home_es/Temas/Turismo+ocio+y+cultura/">Cultura</a>
			</p>
			<p>
				<a href="https://www.deporteyjuventudnavarra.es/es/inicio"
					target="_blank">Deporte</a>
			</p>
			<p>
				<a href="/home_es/Temas/Educacion/">Educación</a>
			</p>
			<p>
				<a href="/home_es/Temas/Empleo+y+Economia/Empleo/">Empleo</a>
			</p>
			<p>
				<a href="/home_es/Temas/Empleo+y+Economia/default.htm">Empresas
					y Profesionales</a>
			</p>
			<p>
				<a
					href="/home_es/Gobierno+de+Navarra/Organigrama/Los+departamentos/Economia+y+Hacienda/Organigrama/Estructura+Organica/Hacienda/">Hacienda</a>
			</p>
			<p>
				<a href="/home_es/Temas/Igualdad+de+genero/default.htm">Igualdad
					de género</a>
			</p>
			<p>
				<a href="/home_es/Temas/Justicia/">Justicia</a>
			</p>
			<p>
				<a href="https://www.deporteyjuventudnavarra.es/es/inicio"
					target="_blank">Juventud</a>
			</p>
			<p>
				<a href="/home_es/Temas/Medio+Ambiente/">Medio Ambiente</a>
			</p>
			<p>
				<a href="/home_es/Temas/Portal+de+la+Salud/default.htm">Salud</a>
			</p>
			<p>
				<a href="/home_es/Temas/Seguridad/">Seguridad</a>
			</p>
			<p>
				<a href="/home_es/Temas/Territorio/default.htm">Territorio y
					Transporte</a>
			</p>
			<p>
				<a href="/home_es/Temas/Vivienda/default.htm">Vivienda</a>
			</p>
			<p>&nbsp;</p>
		</div>

		<div class="col-sm-2 col">
			<span class="titulo"> <a id="ctl00_lnkPieZona4Titulo"
				href="/home_es/Gobierno+de+Navarra/">Gobierno</a>
			</span>
			<p>
				<a
					href="/home_es/Gobierno+de+Navarra/Equipo+de+gobierno/default.htm">Equipo
					de gobierno</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Planes/">Programa de
					legislatura y seguimiento</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Presupuesto/">Presupuesto,
					cuentas&nbsp;y endeudamiento</a>
			</p>
			<p>
				<a
					href="/home_es/Actualidad/Sala+de+prensa/Profesionales/Convocatorias/">Agenda
					de actos</a>
			</p>
			<p>
				<a
					href="/home_es/Gobierno+de+Navarra/Organigrama/Los+departamentos/">Departamentos</a>
			</p>
			<p>
				<a href="http://www.cpen.es/">Corporación Pública Empresarial</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Fundaciones/">Fundaciones
					públicas</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Telefonos+y+direcciones/">Teléfonos
					y direcciones</a>
			</p>
			<p>
				<a href="http://gobiernoabierto.navarra.es/es/transparencia">Transparencia</a>
			</p>
			<p>
				<a href="http://gobiernoabierto.navarra.es/es/participacion">Participación</a>
			</p>
			<p>
				<a href="http://gobiernoabierto.navarra.es/es/open-data">Open
					Data</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Simbolo+oficial/">Símbolo
					Oficial del Gobierno</a>
			</p>
			<p>
				<a href="/home_es/Gobierno+de+Navarra/Sede/">Palacio de Navarra</a>
			</p>
		</div>

		<div class="col-sm-2 col">
			<span class="titulo"> <a id="ctl00_lnkPieZona5Titulo"
				href="/home_es/Actualidad/">Actualidad</a>
			</span>
			<p>
				<a href="http://www.navarra.es/home_es/Actualidad/BON/">BON</a>
			</p>
			<p>
				<a href="/home_es/Actualidad/Sala+de+prensa/Noticias/">Noticias</a>
			</p>
			<p>
				<a href="http://www.saladeprensa.navarra.es/">Ruedas de prensa</a>
			</p>
			<p>
				<a href="/home_es/Actualidad/Sala+de+prensa/Profesionales/">Acreditaciones
					de prensa</a>
			</p>
			<p>
				<a
					href="https://comunicacion.navarra.es/webmedios/ListadoConsultaMedios.jsf">Directorio
					de medios de comunicación</a>
			</p>
			<p>&nbsp;</p>
		</div>
	</div>
</div>