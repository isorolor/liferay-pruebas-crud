<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

<div class="navvv">
	<a class="botonMenu" data-toggle="collapse" href="#collapseExample"
		role="button" aria-expanded="false" aria-controls="collapseExample">
		<img src="${themeDisplay.getPathThemeImages()}/ico-menu.png"
		width:20px height:20px> MENÚ</a> 
	<a class="botonCentral"><img src="${themeDisplay.getPathThemeImages()}/navarrasm.png"></a> 
	<a class="botonDerecha1" data-toggle="collapse" href="#" role="button" aria-expanded="false" aria-controls="collapseExample">
		<img src="${themeDisplay.getPathThemeImages()}/ico-atendemos.png" width:20px height:20px> TE ATENDEMOS |</a>
	
	<a class="botonDerecha2" data-toggle="collapse" href="#collapseExample2"
		role="button" aria-expanded="false" aria-controls="collapseExample2">
		<img src="${themeDisplay.getPathThemeImages()}/ico-idioma.png"
		width:20px height:20px> IDIOMAS |</a> 		
</div>

<ul class="dropdowm-menu">
	<div class="collapse" id="collapseExample">
		<@liferay_portlet["runtime"] portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />		
	</div>
</ul>

<ul class="dropdowm-menu" id="X">
	<div class="collapse" id="collapseExample2">
		<@liferay_portlet["runtime"]
		    instanceId="language-content-viewer"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>	
	</div>
</ul>

<div id="ctl00_Contenido_divBuscador" class="buscador buscador-top" style="height: 86px; margin-top: -10px;">
                <div class="container">
                    <div class="row" style="margin-top: 25px;">
                        <form class="buscador" id="frmSearch" action="/home_es/Indices/Buscador/default.htm" method="get">
                            <fieldset>
                                <label for="q" style="visibility:hidden">buscador</label>
                                <div class="input-group">
                                    <input type="text" name="q" id="q" class="form-control placeholder" value="Buscar en misitio.com" onblur="if(this.value==''){this.value='Buscar en navarra.es'}" onfocus="this.value=''">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>